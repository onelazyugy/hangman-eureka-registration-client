## Hangman Registration Eureka Client
- A sample Eureka client
- http://localhost:8081/actuator/health

# mvn commands
- $ mvn clean install
- $ mvn package
- $ mvn spring-boot:run

# mvn equivalent to gradle
- $mvn package -->	$ gradle assemble
- mvn test -->	$ gradle test
- mvn clean -->	$ gradle clean
- mvn –help	-->	$ gradle –help
- mvn install -->$ gradle install
- mvn –version -->	$ gradle –version