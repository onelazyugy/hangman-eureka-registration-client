package com.viet.le.hangmaneurekaregistrationclient;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HangmanRegistrationEurekaClientController {

    @RequestMapping("/greeting")
    public String greeting() {
        return "Hello from hangman registration eureka client!";
    }


    @RequestMapping("/greeting/{name}")
    public String greetingByName(@PathVariable String name) {
        return "Hello " + name;
    }
}
